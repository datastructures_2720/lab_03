## Lab III: Revision (Recursion)
What is recursion? How to implement a recursive method and when it is a good idea to do so? In this lab, we try to let students find the answers to such questions.  

In this project, we try to implement the following 7 methods and run some comparative experiments on recursive-vs-iterative implementation of the methods.  
```java
int addRec(int a, int b);
int sum(int n);
int sumRec(int n);
int search(long[] arr, long x);
int searchByDividingRec(long[] arr, int from, int to, long x);
int searchByDividing(long[] arr, int from, int to, long x);
int howManyInLine(List<Integer> line, int n);
```

### Objectives:
* Brushing up on students' skill in implementing recursive methods. This will come handy for later topics when more complicated data structures,
such as Binary Trees and Linked Lists, will be introduced.
* Letting students run some experiments with the running time of the methods they implement in both iterative and recursive fashion. They can observe some cases when recursion
is benefitial and when that is not a smart approach.

### Description
The details of this project can be found at [class page](https://www.azim-a.com/teaching/data-structures-2720).

#### Author
* *Azim Ahmadzadeh* - [webpage](https://www.azim-a.com/)
#### Course
* *Data Structures - 2720* - Fall 2018
#### School
* [Computer Science Department](https://www.cs.gsu.edu/) - Georgia State University
#### License
This project is licensed under the GNU General Public License - see the [GPL LICENSE](http://www.gnu.org/licenses/gpl.html) for details.



